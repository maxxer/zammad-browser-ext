/**
 * Reads info from the local storage and displays the tickets
 * @param {*} event 
 */
function renderTicketList(zData) {
    var ticketList = zData.zammadTicketsList;
    document.querySelectorAll(".singleevent").forEach(e => e.parentNode.removeChild(e));

    console.log(ticketList);
    for (var idTicket in ticketList) {
        var ticket = ticketList[idTicket];
        var row = document.createElement('div');
        row.classList.add("singleevent");
        row.innerHTML = ticket.title+ "("+ticket.number+")";
        document.body.appendChild(row);
    }
}

function renderTicketListError(e) {
    document.querySelectorAll(".singleevent").forEach(e => e.parentNode.removeChild(e));
    var row = document.createElement('div');
    row.classList.add("singleevent");
    row.innerHTML = "Error getting ticket list: "+e.message;
    document.body.appendChild(row);
    console.error(e);
}

// Startup
gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(renderTicketList, renderTicketListError);