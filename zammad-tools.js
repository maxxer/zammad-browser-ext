
/**
 * makes an API call to Zammad
 */
function zammadCall(nickname, call, callback) {
    if (zammadOptions == null) {
        console.error("No credentials");
        return;
    }

    zammadHttpRequest[nickname] = new XMLHttpRequest();
    zammadHttpRequest[nickname].open("GET", zammadOptions.url+"/api/v1/"+call);
    zammadHttpRequest[nickname].setRequestHeader("Content-Type", "application/json");
    zammadHttpRequest[nickname].setRequestHeader("Authorization", "Token token="+zammadOptions.token);
    zammadHttpRequest[nickname].onload = callback;
    zammadHttpRequest[nickname].onerror = zammadError;
    zammadHttpRequest[nickname].send();
}

/**
 * Handless zammad call error
 * @param {*} e 
 */
function zammadError(e) {
    chrome.browserAction.setBadgeText({
        'text': "E"
    });
    if (window.console) {
        console.error("Error when getting Zammad data: ");
        console.error(e);
    }
}

