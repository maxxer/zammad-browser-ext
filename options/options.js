const urlInput = document.querySelector("#url");
const tokenInput = document.querySelector("#token");
const pollingInput = document.querySelector("#polling_delay");
const saveButton = document.querySelector("#save_button");

/*
Store the currently selected settings using browser.storage.local.
*/
function storeSettings() {
  if (window.console) {
    console.log("Stored token "+tokenInput.value);
  }
  browser.storage.local.set({
    zammadCredentials: {
      url: urlInput.value,
      token: tokenInput.value,
      polling_delay: pollingInput.value
    }
  });
}

/*
Update the options UI with the settings values retrieved from storage,
or the default settings if the stored settings are empty.
*/
function updateUI(restoredSettings) {
  tokenInput.value = restoredSettings.zammadCredentials.token || "";
  pollingInput.value = restoredSettings.zammadCredentials.polling_delay || "";
  urlInput.value = restoredSettings.zammadCredentials.url || "";
}

function onError(e) {
  console.error(e);
}

/*
On opening the options page, fetch stored settings and update the UI with them.
*/
const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(updateUI, onError);

/*
On blur, save the currently selected settings.
*/
tokenInput.addEventListener("blur", storeSettings);
urlInput.addEventListener("blur", storeSettings);
pollingInput.addEventListener("blur", storeSettings);
saveButton.addEventListener("click", storeSettings);
