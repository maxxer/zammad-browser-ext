// Default polling delay, in minutes
var DELAY = 4;
var zammadOptions = null;
// Array of XMLHttpRequest()
var zammadHttpRequest = Array;
var firstRun = true;

function loadOptions(zOpts) {
    zammadOptions = zOpts.zammadCredentials;
    if (typeof zammadOptions != "undefined" && Object.keys(zammadOptions).length != 0) {
        DELAY = parseFloat(zammadOptions.polling_delay);
    }
    // Instantiate alarm with the choosen timer
    if (firstRun) {
        browser.alarms.clearAll();
        browser.alarms.create("zammad", {periodInMinutes: DELAY});
        firstRun = false;
    }
}

/**
 * Perform API calls to Zammad
 */
function refreshZammadNotifications() {
    zammadCall("notifications", "online_notifications", handleZammadNotificationsResponse);
    zammadCall("search", "search?query=state:new%20OR%20state:open&limit=5", handleZammadSearchResponse);
}

/**
 * Handles response to the "online_notiifcations" API call.
 * Shows a desktop notification and updates the unread count badge on the icon
 * @param {*} event
 */
function handleZammadNotificationsResponse(event) {
    if (checkResponseCode("notifications") == false) {
        return;
    }
    var jsonResponse = JSON.parse(zammadHttpRequest["notifications"].response);

    var unreadCount = 0;
    for (x in jsonResponse) {
        if (jsonResponse[x].seen == false) {
            unreadCount++;
        }
    }

    var popupNumber = "";
    if (unreadCount > 0) {
        browser.notifications.create({
            "type": "basic",
            "iconUrl": browser.extension.getURL("images/zammad-logo-48.png"),
            "title": "Unread events on Zammad",
            "message": "You have "+unreadCount+" unread event(s) on Zammad"
        });
        popupNumber = String(unreadCount);
    }
    // Update counter over the icon
    browser.browserAction.setBadgeText({
        'text': popupNumber
    });
    zammadHttpRequest["notifications"] = null;
}

/**
 * Handles response to the "search" API call.
 * Stores the tickets information in local storage to be used in the popup
 * @param {*} event
 */
function handleZammadSearchResponse(event) {
    if (checkResponseCode("search") == false) {
        return;
    }
    var jsonResponse = JSON.parse(zammadHttpRequest["search"].response);

    browser.storage.local.set({
        zammadTicketsList: jsonResponse.assets.Ticket
    });
    zammadHttpRequest["search"] = null;
}

function checkResponseCode(nickname) {
    var r = zammadHttpRequest[nickname].status;
    if (r == 404) {
        browser.browserAction.setBadgeText({
            'text': "URL"
        });
        return false;
    } else if (r == 401) {
        browser.browserAction.setBadgeText({
            'text': "Auth"
        });
        return false;
    }
    return true;
}

/**
 * On alarm, show the page action.
 */
browser.alarms.onAlarm.addListener((alarm) => {
    refreshZammadNotifications();
    gettingStoredSettings = browser.storage.local.get();
    gettingStoredSettings.then(loadOptions, onError);
});

function onError(e) {
    console.error("Error getting stored settings:");
    console.error(e);
}

// Main stuff
gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(loadOptions, onError);