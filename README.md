# Zammad ticketing system browser extension

I developed this extension to annoy agents when they have unread notifications. 

The goal is to help agents who are not 100% committed to helpdesk to keep track of new events. 
For instance when you're fully concentrated on developing and you miss a desktop notification, 
or you're AFK and come back to something else which is not helpdesk.

It's still **VERY BARE**, needs a lot of improvement.

The extension uses token for accessing Zammad. To create one go to your *user profile > Access token* and ask for a new one. Check the following permissions:
* `user_preferences.notifications` 
* `ticket.agent`

## Author

Lorenzo Milesi <lorenzo@mile.si>
